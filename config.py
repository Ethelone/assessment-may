import os
from dotenv import load_dotenv
import configparser

load_dotenv()

class Config:
    config = configparser.ConfigParser()
    config.read('config.cfg')

    PROJECT_NAME = config['API']['project_name']
    DEFAULT_URL = config['API']['url']
    PORT =  config['API'].getint('port')

    LOG_FILE_DIRECTORY = config['LOGGING']['log_file_directory']

    
    DB_URL = os.getenv("POSTGRES_URL")
    DB_PORT = os.getenv("POSTGRES_PORT")
    DB_USER = os.getenv("POSTGRES_USER")
    DB_PW = os.getenv("POSTGRES_PASSWORD")
    DB_MAINT = os.getenv("POSTGRES_DB")

    REDIS_HOST = config['REDIS']['redis_host']
    REDIS_PORT = config['REDIS'].getint('redis_port')
