import redis
from config import Config
from log_config import account_logger as log

redis_client = redis.StrictRedis(host=Config.REDIS_HOST, port=Config.REDIS_PORT, decode_responses=True)

def sendMessage(channel, message):
    try:
        redis_client.publish(channel=channel, message=message)
        redis_client.close()
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Failed to sendMessage \n error: {e} \n filename: {filename} - line: {line_number}")
        raise e