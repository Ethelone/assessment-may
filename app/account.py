from . import hashing, redis_sender
from models import account as models
from models import mutasi as mutasi_model
from datastore import account as datastore
from datastore.datastore import openTx
from log_config import account_logger as log
from fastapi import HTTPException
import json
from datetime import datetime

def daftar(request: models.DaftarRequest) -> models.DaftarResponse:
    try:
        session = openTx()
        account_check = models.Account(**request.model_dump())
        account = datastore.getAccount(session, account_check)
        if account.nik == request.nik:
            raise HTTPException(400, "NIK sudah digunakan")
        
        if account.no_hp == request.no_hp:
            raise HTTPException(400, "Nomor HP sudah digunakan")
        
        request.pin = hashing.hashpass(request.pin)
        no_rek = datastore.daftar(session, request)
        response = models.DaftarResponse(message="Success", no_rekening=no_rek)

        session.commit()
        session.close()
        log.info(f"Daftar sukses dengan norek: {no_rek}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def deposit(request: models.DepositRequest) -> models.DepositResponse:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request.no_rekening)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        tgl_transaksi = str(datetime.now())

        #Redis
        message = mutasi_model.TransactionRequest(
            no_rekening=request.no_rekening,
            nominal=request.nominal,
            jenis_transaksi="C",
            tanggal_transaksi=tgl_transaksi
        )

        json_message = json.dumps(message.model_dump(), indent=2)
        redis_sender.sendMessage(channel="mutasi", message=json_message)
        #--

        response = models.DepositResponse(saldo=(account.saldo + request.nominal))
        session.commit()
        session.close()
        log.info(f"Deposit sukses")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def withdraw(request: models.WithdrawRequest) -> models.WithdrawResponse:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request.no_rekening)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        if account.saldo < request.nominal:
            raise HTTPException(400, "Saldo tidak cukup")

        tgl_transaksi = str(datetime.now())

        #Redis
        message = mutasi_model.TransactionRequest(
            no_rekening=request.no_rekening,
            nominal=request.nominal,
            jenis_transaksi="D",
            tanggal_transaksi=tgl_transaksi
        )

        json_message = json.dumps(message.model_dump(), indent=2)
        redis_sender.sendMessage(channel="mutasi", message=json_message)
        #--
        
        response = models.DepositResponse(saldo=(account.saldo - request.nominal))
        session.commit()
        session.close()
        log.info(f"Withdraw sukses")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def getBalance(request: str) -> float:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        session.commit()
        session.close()
        log.info(f"Getsaldo sukses dengan norek: {account.no_rekening}")
        return account.saldo
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e
    
def getTransactions(request: str) -> models.MutationResponse:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        transactions = datastore.getTransactions(session, request)
        response = models.MutationResponse(
            mutasi = transactions
        )
        
        session.commit()
        session.close()
        log.info(f"Mutasi sukses dengan norek: {account.no_rekening}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e