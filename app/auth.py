from . import hashing
from models import auth as models
from datastore import auth as datastore
from datastore.datastore import openTx
from fastapi import HTTPException
from log_config import auth_logger as log


def cekPin(request: models.CekpinRequest):
    try:
        session = openTx()
        result = datastore.cekPin(session, request)
        if hashing.checkPin(request.pin, result.pin) == False:
            raise HTTPException(401, "Pin salah")
        log.info(f"User {result.no_rekening} authorized")
        session.commit()
        session.close()
        return True
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

