# Backend Assessment Test - May 2024

## Table of Contents

- [About](#about)
- [Setup](#setup)
- [Features](#features)

## About <a name = "about"></a>

Assessment test untuk bulan May 2024 yakni suatu service mutasi. Merupakan fork dari Assessment Test Feb 2024 yang dimodifikasi untuk memenuhi requirements
<br/>
<br/>
Stacks: Python, Uvicorn FastAPI, Redis, PSQL

## Setup <a name = "setup"></a>

### Prerequisites

Software yang diperlukan untuk running

```
Docker, Python, Postman (Optional, highly recommended), RDBMS untuk Postgres (Optional)
```

### Installing


```
1. Clone repo ke local
2. Make sure docker sudah running
3. (Opsional) Ubah [- RUN_TESTS=false] menjadi true untuk melakukan unit testing on run
4. run ["docker-compose build] pada terminal
```

### Running

```
run ["docker-compose up] pada terminal
```
Jika semua lancar, seharusnya app akan memunculkan seperti dibawah pada terminal
```
fastapi_service_may   | INFO:     Started server process [1]
fastapi_service_may   | INFO:     Waiting for application startup.
fastapi_service_may   | INFO:     Application startup complete.
fastapi_service_may   | INFO:     Uvicorn running on http://0.0.0.0:8001 (Press CTRL+C to quit)
```
### Disclaimer
```
.env sengaja tidak masuk gitignore untuk mempermudah penilaian
segala perubahan .env maupun .cfg harap di perhatikan juga pada dockerfilenya
```

## Features <a name = "features"></a>

### Daftar
```
Type: POST
Endpoint: /account/daftar
Request Body: {
    "nama":STRING,
    "nik":UNIQUE_STRING,
    "no_hp":UNIQUE_STRING,
    "pin":STRING
}
```

### Tabung
```
Type: POST
Endpoint: /account/tabung
Request Headers: 
    Key-Value Pair:
        pin
        no_rekening

Request Body: {
    "no_rekening": STRING,
    "nominal": FLOAT
}
```

### Tarik
```
Type: POST
Endpoint: /account/tarik
Request Headers: 
    Key-Value Pair:
        pin
        no_rekening

Request Body: {
    "no_rekening": STRING,
    "nominal": FLOAT
}
```

### Saldo
```
Type: GET
Endpoint: /account/saldo/{no_rekening}
Request Headers: 
    Key-Value Pair:
        pin
        no_rekening
```

### Mutasi
```
Type: GET
Endpoint: /account/mutasi/{no_rekening}
Request Headers: 
    Key-Value Pair:
        pin
        no_rekening
```

