from sqlalchemy import insert, update, func, String
from models import mutasi as models
from log_config import mutasi_logger as log
import datastore.tables as tables
from sqlalchemy.orm import Session

def createTransaction(session: Session, request: models.TransactionRequest) -> models.TransactionResponse:
    log.info(f"Transaction from acct no: {request.no_rekening}")
    try:
        keterangan = f"Setor tunai acct_no: {request.no_rekening} nominal: {request.nominal}"
        if request.jenis_transaksi == "D":
            keterangan = f"Tarik tunai acct_no: {request.no_rekening} nominal: {request.nominal}"
            
        #Create trx
        transaction_table = tables.transaction
        statement = insert(transaction_table).values(
            no_rekening = request.no_rekening,
            kode_transaksi = request.jenis_transaksi,
            nominal = request.nominal,
            keterangan = keterangan,
            waktu = request.tanggal_transaksi
            ).returning(
                transaction_table.c.id_transaksi
            )
        result = session.execute(statement).fetchone()
        id_transaksi = result[0]

        response = models.TransactionResponse(id_transaksi=id_transaksi)
        session.commit()
        session.close()
        return response

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e
  
def processTransaction(session: Session, no_rekening: str, nominal: float):
    log.info(f"Process Transaction for acct_no {no_rekening}")
    try:
        account_table = tables.account
        statement = (
            update(account_table)
            .where(account_table.c.no_rekening == no_rekening)
            .values(
                saldo=account_table.c.saldo + nominal)
        )
        session.execute(statement)
        return
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e
