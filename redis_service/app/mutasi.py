import redis
from redis_service.datastore import mutasi as datastore
from datastore.datastore import openTx
from models import mutasi as models
from log_config import mutasi_logger as log
from config import Config
import json

async def mutasi_subscribe():
    try:
        redis_client = redis.StrictRedis(host=Config.REDIS_HOST, port=Config.REDIS_PORT, decode_responses=True)
        pubsub = redis_client.pubsub()
        pubsub.subscribe('mutasi')
        
        for message in pubsub.listen():
            if message['type'] == 'message':
                await process_mutasi(message['data'])
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Mutasi subscribe error: {e} \n filename: {filename} - line: {line_number}")

async def process_mutasi(message):
    log.info(f"Mutasi triggered: {message}")
    try:
        session = openTx()
        json_message = json.loads(message)
        mutasi = models.TransactionRequest(**json_message)
        #Create transaksi
        id_transaksi = datastore.createTransaction(session=session, request=mutasi)

        #Process transaksi
        if mutasi.jenis_transaksi == 'C':
            datastore.processTransaction(session=session, no_rekening=mutasi.no_rekening, nominal=mutasi.nominal)
        else:
            datastore.processTransaction(session=session, no_rekening=mutasi.no_rekening, nominal=-mutasi.nominal)

        session.commit()
        session.close()
        log.info(f"Mutasi success with id_transaction: {id_transaksi}")
        return

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Mutasi error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e
