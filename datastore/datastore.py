from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from config import Config
from .tables import metadata
from log_config import main_logger as log

engine = create_engine(
    f"postgresql://{Config.DB_USER}:{Config.DB_PW}@{Config.DB_URL}:{Config.DB_PORT}/{Config.DB_MAINT}"
)

def dbSetup():
    try:
        metadata.create_all(engine)
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Failed to initate DB \n error: {e} \n filename: {filename} - line: {line_number}")

def openTx():
    try:
        session = Session(bind=engine)
        return session
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Failed to initate transaction \n error: {e} \n filename: {filename} - line: {line_number}")