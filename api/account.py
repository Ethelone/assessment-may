from fastapi import APIRouter, Depends, Request
from app import account as app
from models import account as models
from log_config import account_logger as log

router = APIRouter()

@router.post("/daftar")
async def daftar(request: models.DaftarRequest):
    log.info(f"Daftar Request for NIK: {request.nik}")
    try:
        response = app.daftar(request)
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"API error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
@router.post("/tabung")
async def tabung(request: models.DepositRequest):
    log.info(f"Tabung Request for acct: {request.no_rekening}")
    try:
        response = app.deposit(request)
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"API error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
@router.post("/tarik")
async def tarik(request: models.WithdrawRequest):
    log.info(f"Tarik Request for acct: {request.no_rekening}")
    try:
        response = app.withdraw(request)
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"API error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
@router.get("/saldo/{no_rekening}")
async def getsaldo(no_rekening: str):
    log.info(f"Cek Saldo Request acct: {no_rekening}")
    try:
        response = app.getBalance(no_rekening)
        return {"saldo":response}
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"API error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
@router.get("/mutasi/{no_rekening}")
async def getmutasi(no_rekening: str):
    log.info(f"Cek mutasi Request acct: {no_rekening}")
    try:
        response = app.getTransactions(no_rekening)
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"API error: {e} \n filename: {filename} - line: {line_number}")
        raise e