from pydantic import BaseModel

class TransactionRequest(BaseModel):
    no_rekening: str
    nominal: float
    jenis_transaksi: str
    tanggal_transaksi: str = ""

class TransactionResponse(BaseModel):
    id_transaksi: str

