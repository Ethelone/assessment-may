from pydantic import BaseModel
from typing import List

class DaftarRequest(BaseModel):
    nama: str
    nik: str
    no_hp: str
    pin: str

class DaftarResponse(BaseModel):
    message: str
    no_rekening: str

class DepositRequest(BaseModel):
    no_rekening: str
    nominal: float

class DepositResponse(BaseModel):
    saldo: float

class WithdrawRequest(BaseModel):
    no_rekening: str
    nominal: float

class WithdrawResponse(BaseModel):
    saldo: float

class Account(BaseModel):
    no_rekening: str = ''
    nama: str = ''
    nik: str = ''
    no_hp: str = ''
    saldo: float = 0.0

class Transaction(BaseModel):
    id_transaksi: str
    waktu: str
    kode_transaksi: str
    nominal: float
    keterangan: str

class MutationResponse(BaseModel):
    mutasi: List[Transaction] = []
