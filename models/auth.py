from pydantic import BaseModel

class CekpinRequest(BaseModel):
    no_rekening: str
    pin: str

class CekpinResponse(BaseModel):
    no_rekening: str
    pin: str
