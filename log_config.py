import logging
from logging.handlers import TimedRotatingFileHandler
from config import Config
import os

def configure_logging(logger_name, log_file_name):
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    log_file_path = os.path.join(Config.LOG_FILE_DIRECTORY, log_file_name)
    os.makedirs(Config.LOG_FILE_DIRECTORY, exist_ok=True)

    file_handler = TimedRotatingFileHandler(
        log_file_path,
        when='midnight',
        interval=1,
        backupCount=5
    )

    file_handler.setLevel(logging.DEBUG)  # Set to DEBUG or INFO
    file_handler.setFormatter(formatter)

    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)  # Set to DEBUG or INFO
    logger.addHandler(file_handler)

    return logger

# Initialize loggers
main_logger = configure_logging("main_logger", "main.log")
account_logger = configure_logging("account_logger", "account.log")
mutasi_logger = configure_logging("mutasi_logger", "mutasi.log")
auth_logger = configure_logging("auth_logger", "auth.log")
redis_logger = configure_logging("redis_logger", "redis.log")