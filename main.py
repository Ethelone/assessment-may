from fastapi import FastAPI
from api.router import router
import uvicorn
from config import Config
from log_config import main_logger as log
from datastore.datastore import dbSetup
from api.auth import authenticate_user

app = FastAPI(title=Config.PROJECT_NAME)
app.include_router(router)
app.middleware("http")(authenticate_user)

if __name__ == "__main__":
    try:
        dbSetup()
        log.info("Service started") 
        uvicorn.run(app, host=Config.DEFAULT_URL, port=Config.PORT)
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Fatal error occured: {e} \n file: {filename} - line: {line_number}")
    finally:
        log.info("Service stopped")
