import asyncio
from redis_service.app.mutasi import mutasi_subscribe
from log_config import redis_logger as log

async def journal():
    await mutasi_subscribe()

if __name__ == "__main__":
    init_mess = "Redis service started"
    log.info(init_mess)
    print(init_mess)

    loop_running = asyncio.Event()
    try:
        asyncio.run(journal())
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Redis error: {e} \n filename: {filename} - line: {line_number}")
    finally:
        loop_running.set()
