import pytest
from httpx import AsyncClient, ASGITransport
from fastapi.testclient import TestClient
from main import app
import models
import random
from config import Config as cfg
import models.account

baseUrl = f"http://{cfg.DEFAULT_URL}:+{cfg.PORT}"
global no_rekening

@pytest.fixture
def anyio_backend():
    return 'asyncio'

@pytest.mark.anyio
async def test_daftar():
    global no_rekening
    async with AsyncClient(transport=ASGITransport(app=app), base_url=baseUrl) as ac:
        request = {
            "nama":"test",
            "nik":str(random.randint(100000,999999)),
            "no_hp":str(random.randint(100000,999999)),
            "pin":"123456"
        }

        response = await ac.post("/account/daftar", json=request)

        assert response.status_code == 200
        resp_data =  response.json()
        daftar_response = models.account.DaftarResponse(**resp_data)
        assert daftar_response.no_rekening != ""
        no_rekening = daftar_response.no_rekening

    print(f"Daftar Passed | Norek Testing {no_rekening}")

@pytest.mark.anyio
async def test_tabung_rek_kenal():
    global no_rekening
    async with AsyncClient(transport=ASGITransport(app=app), base_url=baseUrl) as ac:
        request = {
            "no_rekening": no_rekening,
            "nominal": 20000.0
        }

        header = {
            "pin":"123456",
            "no_rekening":no_rekening
        }

        response = await ac.post("/account/tabung", json=request, headers=header)

        assert response.status_code == 200

        resp_data =  response.json()
        tabung_response = models.account.DepositResponse(**resp_data)
        assert int(tabung_response.saldo) == 20000

    print(f"Tabung Rekening Dikenali Passed | Norek Testing {no_rekening}")

@pytest.mark.anyio
async def test_tabung_rek_tidak_kenal():
    async with AsyncClient(transport=ASGITransport(app=app), base_url=baseUrl) as ac:
        request = {
            "no_rekening": "0",
            "nominal": 20000.0
        }

        header = {
            "pin":"123456",
            "no_rekening":"0"
        }

        response = await ac.post("/account/tabung", json=request, headers=header)

        assert response.status_code == 400

        
    print(f"Tabung Rekening Tidak Dikenali Passed")

@pytest.mark.anyio
async def test_tarik_cukup():
    global no_rekening
    async with AsyncClient(transport=ASGITransport(app=app), base_url=baseUrl) as ac:
        request = {
            "no_rekening": no_rekening,
            "nominal": 10000.0
        }

        header = {
            "pin":"123456",
            "no_rekening":no_rekening
        }

        response = await ac.post("/account/tarik", json=request, headers=header)

        assert response.status_code == 200

        resp_data =  response.json()
        tarik_response = models.account.WithdrawResponse(**resp_data)
        assert int(tarik_response.saldo) == 10000

        
    print(f"Tarik Saldo Cukup Passed | Norek Testing {no_rekening}")

@pytest.mark.anyio
async def test_tarik_tidak_cukup():
    global no_rekening
    async with AsyncClient(transport=ASGITransport(app=app), base_url=baseUrl) as ac:
        request = {
            "no_rekening": no_rekening,
            "nominal": 1000000.0
        }

        header = {
            "pin":"123456",
            "no_rekening":no_rekening
        }

        response = await ac.post("/account/tarik", json=request, headers=header)

        assert response.status_code == 400
        
    print(f"Tarik Saldo Tidak Cukup Passed")

